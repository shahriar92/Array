<?php
//first example
$friends = array(
    "john" => array ("location" => "us", "age" => "29", "profession" => "designner"),
    "shahriar" => array ("location" => "bangladesh", "age" => "24", "profession" => "student"),
    "sachin" => array ("location" => "india", "age" => "45", "profession" => "cricketer")
);
echo "he is " . $friends ['shahriar']['age'] .  "years old" . " and " . "his profession is "  . $friends['shahriar']['profession'];
echo "<br>";
echo "<br>";
?>

<?php
//2nd example
$cars = array(
    array("volvo", 22,18),
    array("BMW",15,13),
    array("saab", 5,2),
    array("Land Rover",17,15)
);

foreach ($cars as $model){
    foreach ($model as $models => $m){
        echo "<pre>" ;
        print_r($models . " = " . $m);
        echo "</pre>";
    }
}
?>