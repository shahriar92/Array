
        <?php
        //example one
        $name = array("shahriar","Tamim","sakib");
        echo "My name is -> " . $name[0];
        echo "<br>";
        echo "<br>";
        ?>

<?php
//example two
$product[0] = "Rice";
$product[1] = "tea";
$product[2]= "fish";
$product[3] = "meat";
echo "I like ". $product[0]. " , " . " and ". $product[3]. "." ;
echo "<br>";
echo "<br>";
?>

<?php
//example 3
$array = array(
    1 => "a",
    "1" => "b",
    2.5 => "c",
    TRUE => "d",
);
var_dump($array);
echo "<br>";
echo "<br>";
?>

<?php
//example 4
$age = array("rahim"=>"35","karim"=>"45","abul"=>"24");
echo "shahriar is ".$age['abul']. "years old.";
echo "<br>";
echo "<br>";
?>

<?php
//example 5
$run = array("Sakib"=>"45", "Tamim"=>"5", "Musfiq"=>"30");

foreach($run as $batsman_name => $batsman_run) {
     echo "Batsman=" . $batsman_name . ", Run=" . $batsman_run;
     echo "<br>";
}

?>